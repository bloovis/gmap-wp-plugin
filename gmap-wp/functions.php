<?php
/*

Plugin Name: Google Map Link Shortcode

*/

/* Add Your Code Snippets Below This Line. Make Sure to Document What they Do With Comments Like This */

/**
 * Get the URL for a Google map given an address.
 * @param array $atts
 * @return string
 */
function em_google_map_link($args){
        $args = (array) $args;
        return '<a href="https://www.google.com/maps/place/' .
                urlencode($args['addr']) . '"' .
                ' target="_blank">' .
		$args['text'] .
		'</a>';
}
add_shortcode ( 'google_map_link', 'em_google_map_link');
?>
