# Google Map Link Plugin for Wordpress/ClassicPress

This is a very simple plugin that creates a shortcode for
inserting a Google Maps link into a page.  This link
does NOT embed a map and does not require a Google Maps
API key.  Instead, it opens a new tab/window for a map
using the specified street address and link text.

As an example, the following shortcode will open a map for the White
House:

    [google_map_link addr="1600 Pennsylvania Avenue, Washington, DC" text="Map of White House"]
